usage: gif.py [-h] [-i INPUT] [-o OUTPUT] [-f FPS] [-p DEFINITION]

Make GIF from videos.

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        video input [path,begin(HH:MM:SS),duration(HH:MM:SS)]
  -o OUTPUT, --output OUTPUT
                        gif output
  -f FPS, --fps FPS     gif fps (default=30)
  -p DEFINITION, --definition DEFINITION
                        gif definition (default=640)
