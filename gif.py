#!/bin/python

import argparse
import os
import subprocess

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Make GIF from videos.")
    parser.add_argument("-i", "--input", action="append", type=str, help="video input [path,begin(HH:MM:SS),duration(HH:MM:SS)]")
    parser.add_argument("-o", "--output", type=str, help="gif output", default="output.gif")
    parser.add_argument("-f", "--fps", type=int, help="gif fps (default=30)", default=30)
    parser.add_argument("-p", "--definition", type=int, help="gif definition (default=640)", default=640)
    args = parser.parse_args()

    if not args.input :
        parser.print_help()
        parser.exit()

    outputName, outputExtension = os.path.splitext(args.output)
    if outputExtension != ".gif" :
        args.output += ".gif"

    tmpDirectory = os.getcwd() + "/tmp"
    if not os.path.exists(tmpDirectory):
        os.makedirs(tmpDirectory)

    videoNames = []
    for input in args.input :
        video, begin, duration = input.split(',')

        videoName, videoExtension = os.path.splitext(video)
        videoName = os.path.basename(videoName)
        videoName += "Begin" + begin
        videoName += "Duration" + duration
        videoName = tmpDirectory + '/' + videoName + videoExtension
        videoNames.append(videoName);

        subprocess.call(["ffmpeg", "-ss", begin, "-i", video, "-t", duration, "-c", "copy", "-an", "-y", videoName])

    
    concatFileName = tmpDirectory + '/' + os.path.splitext(args.output)[0] + ".txt"
    concatFile = open(concatFileName, 'w')
    for videoName in videoNames :
        concatFile.write("file '" + videoName + "'\n")
    concatFile.close()

    concatVideoName = tmpDirectory + '/' + os.path.splitext(args.output)[0] + ".mov"
    subprocess.call(["ffmpeg", "-f", "concat", "-safe", '0', "-y", "-i", concatFileName, "-c", "copy", concatVideoName])


    paletteFileName = tmpDirectory + '/' + os.path.splitext(args.output)[0] + "_palette.png"
    filters = "fps=" + str(args.fps) + ",scale=" + str(args.definition) + ":-1:flags=lanczos"

    subprocess.call(["ffmpeg", "-v", "warning", "-i", concatVideoName, "-vf", filters + ",palettegen", "-y", paletteFileName])
    subprocess.call(["ffmpeg", "-v", "warning", "-i", concatVideoName, "-i", paletteFileName, "-lavfi", filters + " [x]; [x][1:v] paletteuse", "-y", args.output])
